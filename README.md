# Muvi - Pipeline

This Pipelie is based on docker container deployemnt

1. Before running pipeline, you have to pass below varibale as mandatory

    1. DOCKER_USERNAME: <Dockerhub Account_Name or Repo_ID>
    2. REPO_NAME: <Image_Name>
    3. IMAGE_TAG: "${CI_PIPELINE_ID}"
    4. CONTAINERPORT: <Container_Port>
    5. DOCKER_PASSWD: <PASS>

    Reference:-
    
    include:
        
        project: 'muvi-pipeline/development'
        ref: 'main'
        file: 'muvi-pipeline.gitlab-ci.yml'
        tags:
          - <runner_name>
    
    variables:

        DOCKER_USERNAME: ABCDE
        REPO_NAME: nginx
        IMAGE_TAG: "${CI_PIPELINE_ID}"
        CONTAINERPORT: 80
        DOCKER_PASSWD: ABCDE

2. Pipeline will run four stages
    1. dev_validate
    2. dev_build
    3. dev_test
    4. dev_deploy(Manually trigger)

3. You can check your results on dev_test stage itself, Once you statisfied then you can go with dev_deploy stage.

